let results = { "1": 0, "2": 0 };
let isListening = true;

let options = {
	options: {
		clientId: "8ju2p4yaaaijh00vntwk79k8rxqd9l",
		debug: false
	},
	connection: {
		secure: true,
		reconnect: true
	},
	channels: ["#cellbit"]
};

let client = new tmi.client(options);

// Connect the client to the server..
client.connect();
client.on("connected", (address, port) => {
	// Do your stuff.
	$("#chat").append("<div>Connected</div>");
});
client.on("connecting", (address, port) => {
	// Do your stuff.
	$("#chat").append("<div>Conecting...</div>");
});
client.on("disconnected", reason => {
	// Do your stuff.
	$("#chat").append("<div>Disconnected</div>");
});

client.on("message", (channel, userstate, message, self) => {
	// Don't listen to my own messages..
	if (self) return;

	// Handle different message types..
	switch (userstate["message-type"]) {
		case "action":
			// This is an action message..
			break;
		case "chat":
			// This is a chat message..
			//console.log(message);
			//$("#chat").append("<div><b>" + userstate["display-name"] + "</b>: " + message + "</div>");
			if (isListening) {
				if ("1" == convertToSlug(message)) {
					results["1"]++;
					//$("#chat").append(results);
				} else if ("2" == convertToSlug(message)) {
					results["2"]++;
					//$("#chat").append(results);
				}
			}
			break;
		case "whisper":
			// This is a whisper..
			break;
		default:
			// Something else ?
			break;
	}
	$("#chat").scrollTop($("#chat")[0].scrollHeight - $("#chat")[0].clientHeight);
});

function convertToSlug(string) {
	return string
		.toString()
		.toLowerCase()
		.replace(/[àÀáÁâÂãäÄÅåª]+/g, "a") // Special Characters #1
		.replace(/[èÈéÉêÊëË]+/g, "e") // Special Characters #2
		.replace(/[ìÌíÍîÎïÏ]+/g, "i") // Special Characters #3
		.replace(/[òÒóÓôÔõÕöÖº]+/g, "o") // Special Characters #4
		.replace(/[ùÙúÚûÛüÜ]+/g, "u") // Special Characters #5
		.replace(/[ýÝÿŸ]+/g, "y") // Special Characters #6
		.replace(/[ñÑ]+/g, "n") // Special Characters #7
		.replace(/[çÇ]+/g, "c") // Special Characters #8
		.replace(/[ß]+/g, "ss") // Special Characters #9
		.replace(/[Ææ]+/g, "ae") // Special Characters #10
		.replace(/[Øøœ]+/g, "oe") // Special Characters #11
		.replace(/[%]+/g, "pct") // Special Characters #12
		.replace(/\s+/g, "-") // Replace spaces with -
		.replace(/[^\w\-]+/g, "") // Remove all non-word chars
		.replace(/\-\-+/g, "-") // Replace multiple - with single -
		.replace(/^-+/, "") // Trim - from start of text
		.replace(/-+$/, ""); // Trim - from end of text
}
