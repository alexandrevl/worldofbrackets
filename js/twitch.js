let resultsTwitch = { "1": 0, "2": 0 };
let isListeningTwitch = false;

let optionsTwitch = {
	options: {
		clientId: "8ju2p4yaaaijh00vntwk79k8rxqd9l",
		debug: false
	},
	connection: {
		secure: true,
		reconnect: true
	},
	channels: ["#cellbit"]
};

let client = new tmi.client(optionsTwitch);

// Connect the client to the server..
function connectTwitch() {
	client.connect();
}
function disconnectTwitch() {
	client.disconnect();
}
client.on("connected", (address, port) => {
	// Do your stuff.
	console.log("Connected");
});
client.on("connecting", (address, port) => {
	// Do your stuff.
	console.log("Connecting...");
});
client.on("disconnected", reason => {
	// Do your stuff.
	console.log("Disconnected");
});

client.on("message", (channel, userstate, message, self) => {
	// Don't listen to my own messages..
	if (self) return;

	// Handle different message types..
	switch (userstate["message-type"]) {
		case "action":
			// This is an action message..
			break;
		case "chat":
			// This is a chat message..
			//console.log(message);
			//$("#chat").append("<div><b>" + userstate["display-name"] + "</b>: " + message + "</div>");
			if (isListeningTwitch) {
				//console.log(userstate["display-name"], message);
				if ("1" == convertToSlug(message)) {
					resultsTwitch["1"]++;
					var total = resultsTwitch["1"] + resultsTwitch["2"];
					$("#totalVotes").text("Votes: " + total);
					var percent = _.round((resultsTwitch["1"] * 100) / total, 2);
					$("#resultItem1").text(percent + "%");
					percent = _.round((resultsTwitch["2"] * 100) / total, 2);
					$("#resultItem2").text(percent + "%");
				} else if ("2" == convertToSlug(message)) {
					resultsTwitch["2"]++;
					var total = resultsTwitch["1"] + resultsTwitch["2"];
					$("#totalVotes").text("Votes: " + total);
					var percent = _.round((resultsTwitch["1"] * 100) / total, 2);
					$("#resultItem1").text(percent + "%");
					percent = _.round((resultsTwitch["2"] * 100) / total, 2);
					$("#resultItem2").text(percent + "%");
				}
			}
			break;
		case "whisper":
			// This is a whisper..
			break;
		default:
			// Something else ?
			break;
	}
	//$("#chat").scrollTop($("#chat")[0].scrollHeight - $("#chat")[0].clientHeight);
});

function convertToSlug(string) {
	return string
		.toString()
		.toLowerCase()
		.replace(/[àÀáÁâÂãäÄÅåª]+/g, "a") // Special Characters #1
		.replace(/[èÈéÉêÊëË]+/g, "e") // Special Characters #2
		.replace(/[ìÌíÍîÎïÏ]+/g, "i") // Special Characters #3
		.replace(/[òÒóÓôÔõÕöÖº]+/g, "o") // Special Characters #4
		.replace(/[ùÙúÚûÛüÜ]+/g, "u") // Special Characters #5
		.replace(/[ýÝÿŸ]+/g, "y") // Special Characters #6
		.replace(/[ñÑ]+/g, "n") // Special Characters #7
		.replace(/[çÇ]+/g, "c") // Special Characters #8
		.replace(/[ß]+/g, "ss") // Special Characters #9
		.replace(/[Ææ]+/g, "ae") // Special Characters #10
		.replace(/[Øøœ]+/g, "oe") // Special Characters #11
		.replace(/[%]+/g, "pct") // Special Characters #12
		.replace(/\s+/g, "-") // Replace spaces with -
		.replace(/[^\w\-]+/g, "") // Remove all non-word chars
		.replace(/\-\-+/g, "-") // Replace multiple - with single -
		.replace(/^-+/, "") // Trim - from start of text
		.replace(/-+$/, ""); // Trim - from end of text
}
