var match = 0;
var results = [];
var minimalData = null;
var teams = [];
var matrix = [];
var resultDefault = [];
var database = null;
var uuid = uuidv4();
var idBracket = cleanPath(window.location.pathname);
var imgsWork = null;
var imgs = null;
var bracketsJson = null;
var bracketJson = null;
var chatPlay = {
	status: false,
	isListening: false,
	youtube: null,
	twitch: null
};
function cleanPath(element) {
	$.getJSON("../brackets.json?" + Date.now(), function(json) {
		bracketsJson = json;
	});
	let string = element.slice(1);
	return string.slice(0, string.length - 1);
}

$(function() {
	$('[data-toggle="popover"]').popover();
});

function setMetadata() {
	bracketJson = _.find(bracketsJson.brackets, { idBracket: idBracket });
	$("#title").text(bracketJson.main_title);
	$("#subtitle").text(bracketJson.subtitle);
	$("#ogTitle").attr("content", bracketJson.og_title);
	$("#ogDescription").attr("content", bracketJson.og_description);
	$("#ogImg").attr("content", bracketJson.og_img);
	if (!bracketJson.btn_shuffle) {
		$("#btnShuffle").hide();
	}
	if (!bracketJson.btn_reset) {
		$("#btnReset").hide();
	}
}

function init(imgsToGet) {
	if (imgsToGet == null) {
		$.ajax({
			type: "GET",
			url: "https://worldofbrackets.com/listImgs.php?idBracket=" + idBracket,
			contentType: "application/json",
			cache: false,
			statusCode: {
				200: function(response) {
					imgs = _.orderBy(JSON.parse(response));
					imgsWork = imgs;
					setInit(imgsWork);
					drawBracket();
				}
			}
		});
	} else {
		imgsWork = imgsToGet;
		setInit(imgsToGet);
		drawBracket();
	}
}

function setInit(imgs) {
	setMetadata();
	match = 0;
	results = [];
	minimalData = null;
	teams = [];
	matrix = [];
	for (let index = 0; index < imgs.length; index++) {
		teams.push([
			"<div rel='popover' data-placement='right' data-original-title='Title' data-trigger='hover' class='align-middle' onclick='onClick(" +
				index +
				")' id='item-" +
				index +
				"' height='110px' width='110px' title='" +
				imgs[index].split(".")[0].split("/")[1] +
				"'><img src='/img/" +
				imgs[index] +
				"' class='img-fluid'></div>",
			"<div rel='popover' data-placement='right' data-original-title='Title' data-trigger='hover' class='align-middle' onclick='onClick(" +
				++index +
				")' id='item-" +
				index +
				"' height='110px' width='110px' title='" +
				imgs[index].split(".")[0].split("/")[1] +
				"'><img src='/img/" +
				imgs[index] +
				"' class='img-fluid'></div>"
		]);
		matrix.push([--index, ++index, null]);
	}
	matrix = [matrix];
	//console.log(matrix)
	var layer = 0;
	for (let i = imgs.length / 2; i > 1; i = i / 2) {
		var interResults = [];
		var idMatch = 0;
		for (let index = 1; index <= i; index++) {
			interResults.push([0, 0, layer + "-" + idMatch++]);
		}
		results.push(interResults);
		++layer;
		matrix[layer] = [];
	}
	//Final
	interResults = [];
	idMatch = 0;
	// matrix.push([]);
	interResults.push([0, 0, layer + "-" + idMatch++]);
	results.push(interResults);
	resultDefault = results;
	minimalData = {
		teams: teams,
		results: results
	};
}
$(document).ready(function() {
	init(imgs);
	$("#btnReset").popover("toggle");
	if (getUrlParameter("id")) {
		imgsWork = imgs;
		uuid = getUrlParameter("id");
		getBracket(uuid);
	} else {
		imgsWork = imgs;
		init(imgs);
		window.history.pushState("", "", "/" + idBracket + "/?id=" + uuid);
	}
	$('[data-toggle="tooltip"]').tooltip();
	$("#btnShuffle").click(function() {
		imgsWork = _.shuffle(imgs);
		init(imgsWork);
	});
	$("#btnReset").click(function() {
		uuid = uuidv4();
		window.history.pushState("", "", "/" + idBracket + "/?id=" + uuid);
		imgsWork = imgs;
		init(imgs);
	});
	$("#btnChatPlayModalStart").click(function() {
		resultsTwitch = { "1": 0, "2": 0 };
		chatPlay.isListening = true;
		isListeningTwitch = chatPlay.isListening;
		console.log("Listening: " + chatPlay.twitch);
		$("#chatPlayModalTitle").html("Chat Play - Running poll <img src='https://worldofbrackets.com/img/rec.gif' height=22>");
		$("#btnChatPlayModalStart").hide();
		$("#btnChatPlayModalStop").show();
	});
	$("#btnChatPlayModalStop").click(function() {
		chatPlay.isListening = false;
		isListeningTwitch = chatPlay.isListening;
		console.log("Stop Listening: " + chatPlay.twitch);
		var [layer, idMatch] = match.split("-");
		var nowMatch = parseInt(idMatch);
		var nowLayer = parseInt(layer);
		if (resultsTwitch["1"] > resultsTwitch["2"]) {
			click(matrix[nowLayer][nowMatch][0]);
			$("#chatPlayModal").modal("hide");
		} else if (resultsTwitch["2"] > resultsTwitch["1"]) {
			click(matrix[nowLayer][nowMatch][1]);
			$("#chatPlayModal").modal("hide");
		}
		$("#chatPlayModalTitle").html("Chat Play");
		$("#btnChatPlayModalStart").show();
		$("#btnChatPlayModalStop").hide();
	});
	$("#btnChatTwitchChannel").click(function() {
		if ($("#chatTwitchChannel").val().length > 3) {
			chatPlay.status = true;
			$("#twitchCheck").prop("checked", true);
			chatPlay.twitch = $("#chatTwitchChannel").val();
			$("#twitchCheckLabel").html('<i class="fab fa-twitch" /> ' + chatPlay.twitch);
			optionsTwitch.channels = [chatPlay.twitch];
			chatPlay.status = true;
			connectTwitch();
			$("#chatTwitchModal").modal("hide");
		}
	});
	$("#twitchCheck").click(function() {
		if ($(this).is(":checked")) {
			if (chatPlay.twitch != null) {
				$("#chatTwitchChannel").text(chatPlay.twitch);
			}
			$("#twitchCheck").prop("checked", false);
			$("#chatTwitchModal").modal("show");
		} else {
			chatPlay.status = false;
			disconnectTwitch();
		}
	});
	$("#chatPlayModal").on("hidden.bs.modal", function(e) {
		$("#chatPlayModalTitle").html("Chat Play");
		chatPlay.isListening = false;
		isListeningTwitch = chatPlay.isListening;
		console.log("Stop Listening: " + chatPlay.twitch);
		$("#btnChatPlayModalStart").show();
		$("#btnChatPlayModalStop").hide();
	});
	$("#chatPlayModal").on("show.bs.modal", function(e) {
		$("#totalVotes").text("Votes: 0");
		$("#resultItem1").text("0%");
		$("#resultItem2").text("0%");
	});
	$("#winnerModal").on("show.bs.modal", function() {
		setTimeout(() => {
			window.__sharethis__.initialize();
		}, 1000);
	});
});

function onClick(id) {
	$("#item-" + id).popover("hide");
	if (!chatPlay.status) {
		click(id);
	} else {
		var [layer, idMatch] = match.split("-");
		var nowMatch = parseInt(idMatch);
		var nowLayer = parseInt(layer);
		$("#chatPlayModalItem1").attr("src", "/img/" + imgsWork[matrix[nowLayer][nowMatch][0]]);
		$("#chatPlayModalItem2").attr("src", "/img/" + imgsWork[matrix[nowLayer][nowMatch][1]]);
		$("#name1").html(imgsWork[matrix[nowLayer][nowMatch][0]].split(".")[0].split("/")[1]);
		$("#name2").html(imgsWork[matrix[nowLayer][nowMatch][1]].split(".")[0].split("/")[1]);
		$("#chatPlayModal").modal("show");
	}
}

function click(id) {
	// setTimeout(() => {
	var [layer, idMatch] = match.split("-");
	var nowMatch = parseInt(idMatch);
	var nowLayer = parseInt(layer);
	if (matrix[nowLayer][nowMatch][0] != null && matrix[nowLayer][nowMatch][1] != null) {
		results = resultDefault;
		matrix[nowLayer][nowMatch][2] = id;

		var nextMatch = Math.floor((parseInt(idMatch) + 2) / 2) - 1;
		var nextLayer = parseInt(layer) + 1;

		var matchConfig = [null, null, null];
		if (matrix[nextLayer]) {
			if (matrix[nextLayer][nextMatch]) {
				matchConfig = matrix[nextLayer][nextMatch];
			}

			if (parseInt(idMatch) % 2 > 0) {
				matchConfig[1] = id;
				matrix[nextLayer][nextMatch] = matchConfig;
			} else {
				matchConfig[0] = id;
				matrix[nextLayer][nextMatch] = matchConfig;
			}
		}
		drawMatrix(nowLayer);
	}
}

function drawMatrix(nowLayer) {
	//console.log(matrix);
	results = resultDefault;
	matrix.forEach((layerMatrix, indexLayer) => {
		layerMatrix.forEach((matchMatrix, indexMatch) => {
			if (matchMatrix != null && matchMatrix[2] != null) {
				//console.log(indexLayer, indexMatch);
				if (matchMatrix[2] == matchMatrix[0]) {
					results[indexLayer][indexMatch][0] = 1;
					results[indexLayer][indexMatch][1] = 0;
				} else if (matchMatrix[2] == matchMatrix[1]) {
					results[indexLayer][indexMatch][0] = 0;
					results[indexLayer][indexMatch][1] = 1;
				}
			}
		});
	});
	minimalData.results = results;
	drawBracket();
	var imgWinner = null;

	if (matrix.length - 1 == nowLayer) {
		$("#imgWinner").attr("src", "/img/" + imgsWork[matrix[matrix.length - 1][0][2]]);
		$("#txtWinner").text(imgsWork[matrix[matrix.length - 1][0][2]].split(".")[0].split("/")[1]);
		$("#ogUrl").attr("content", "https://worldofbrackets.com/" + idBracket + "/?id=" + uuid);
		$("#st-1").attr("data-url", "https://worldofbrackets.com/" + idBracket + "/?id=" + uuid);
		$("#winnerModal").modal("show");

		// $.getJSON("https://get.geojs.io/v1/ip/geo.json", function(data) {
		// 	dataToWrite.userData = data;
		// 	setBracket(dataToWrite);
		// });
		imgWinner = imgsWork[matrix[matrix.length - 1][0][2]];
	}
	dataToWrite = {
		uuid: uuid,
		img: imgWinner,
		matrix: matrix,
		imgsWork: imgsWork
	};
	//console.log("test", dataToWrite);
	setBracket(dataToWrite);
}

function matchClick(data) {
	match = data;
}

function drawBracket() {
	$("#bracket-div").bracket({
		teamWidth: bracketJson.teamWidth,
		scoreWidth: 0,
		matchMargin: bracketJson.matchMargin,
		roundMargin: bracketJson.roundMargin,
		centerConnectors: true,
		onMatchHover: matchClick,
		skipConsolationRound: true,
		init: minimalData /* data to initialize the bracket with */
	});
	for (let index = 0; index < imgsWork.length; index++) {
		$("#item-" + index).popover({
			html: true,
			delay: 500,
			content: "<div class='text-center'><img src='/img/" + imgsWork[index] + "' class='img-fluid'></div>"
		});
	}
}
function uuidv4() {
	return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
		var r = (Math.random() * 16) | 0,
			v = c == "x" ? r : (r & 0x3) | 0x8;
		return v.toString(16);
	});
}

function getUrlParameter(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
	var results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function setBracket(dataToWrite) {
	var jsonPost = {
		idBracket: idBracket,
		data: dataToWrite
	};
	//console.log(jsonPost);
	$.ajax({
		type: "POST",
		url: "https://worldofbrackets.com/setBracket.php",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(response) {
				//console.log(response.responseText);
			},
			404: function(resultData) {
				//console.log(resultData);
			}
		}
	});
}

function getBracket(uuid) {
	var jsonPost = {
		idBracket: idBracket,
		uuid: uuid
	};
	$.ajax({
		type: "POST",
		url: "https://worldofbrackets.com/getBracket.php",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(data) {
				//console.log(data.error);
				if (!data.error) {
					imgsWork = imgs;
					//console.log(imgs);
					if (data.imgsWork != undefined && data.imgsWork != null) {
						imgsWork = data.imgsWork;
						imgs = data.imgsWork;
					}
					setInit(imgsWork);
					if (data.matrix != undefined) {
						matrix = data.matrix;
					}
					drawMatrix(0);
				} else {
					imgsWork = imgs;
					init(imgs);
				}
			},
			404: function(resultData) {}
		}
	});
}

function insertParam(key, value) {
	// var separator = (window.location.href.indexOf("?")===-1)?"?":"&";
	// window.location.href = window.location.href + separator + key + "=" + value;
}

// [[1,0], [1,0], [1,0], [1,0], [1,0], [1,0], [1,0], [1,0], [1,0], [1,0], [1,0],[0,1],[0,1],[0,1],[0,1],[0,1],[1,0], [1,0],[0,1],[0,1],[0,1],[0,1],[0,1],[1,0],[1,0],[1,0],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1]],       /* first round */
// [[1,0], [1,0],[0,1],[0,1],[0,1],[0,1],[0,1],[1,0],[1,0], [1,0],[0,1],[0,1],[0,1],[0,1],[0,1],[1,0]],  /* second round */
// [[1,0], [1,0],[0,1],[0,1],[1,0], [1,0],[0,1],[0,1]],  /* third round */
// [[1,0], [0,1],[1,0], [0,1]],   /* fourth round */
// [[1,0], [0,1]], // semi
// [[1,0], [0,1]]   /* final and 3rd place */
