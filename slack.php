<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
header('Access-Control-Allow-Credentials: true');
header('Content-Type: application/json'); 

//Make sure that it is a POST request.
if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0) { 
    throw new Exception('Request method must be POST!');
}

//Make sure that the content type of the POST request has been set to application/json 
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
if (strcasecmp($contentType, 'application/json') != 0) {
    throw new Exception('Content type must be: application/json');
}  
//Receive the RAW post data.
$content = trim(file_get_contents("php://input")); 

//Attempt to decode the incoming RAW post data from JSON.
$decoded = json_decode($content, true);

//If json_decode failed, the JSON is invalid.
if (!is_array($decoded)) {
    throw new Exception('Received content contained invalid JSON!'); 
}
$out = [
    "text" => $decoded["event"]["text"]
];
$url = 'https://prod-62.westus.logic.azure.com:443/workflows/7c22f333e6c5475486737fd1f57208fa/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=9F6thwnTKNDXiFrU6uabvfUID9r6do7yoTzlFH9dTYw';
//$data = array('key1' => 'value1', 'key2' => 'value2');

// use key 'http' even if you send the request to https://...
$options = array(
    'http' => array(
        'header'  => "Content-type: text/html\r\n",
        'method'  => 'POST',
        'content' => $content
    )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
if ($result === FALSE) { /* Handle error */ }

//var_dump($result);
print_r($decoded);