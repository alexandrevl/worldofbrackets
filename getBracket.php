<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
header('Access-Control-Allow-Credentials: true');
header('Content-Type: application/json'); 
include 'cred.php';

//Make sure that it is a POST request.
if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0) { 
    throw new Exception('Request method must be POST!');
}

//Make sure that the content type of the POST request has been set to application/json 
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
if (strcasecmp($contentType, 'application/json') != 0) {
    throw new Exception('Content type must be: application/json');
}  
//Receive the RAW post data.
$content = trim(file_get_contents("php://input")); 

//Attempt to decode the incoming RAW post data from JSON.
$decoded = json_decode($content, true);

//If json_decode failed, the JSON is invalid.
if (!is_array($decoded)) {
    throw new Exception('Received content contained invalid JSON!'); 
}
$idBracket = $decoded['idBracket'];
$uuid = $decoded['uuid'];
$conn = new PDO($dsn); 
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if ($conn) {
    $uuidArray = array("uuid" => $uuid);
    $data = json_encode($uuidArray);;
    try {
        $sql = "select * from dox.find(collection => '" . $idBracket . "', term => '" . $data . "')";
        $stmt = $conn->query($sql);
        //$result = $stmt->fetch()['res'];
        // print_r(strlen($stmt->fetch()['res'])) ;
        // if (strlen($result) > 0) {
        //     print_r($result);
        // } else {
        //     print_r(null);
        // }
        //echo $stmt->fetch()[res];
        while ($row = $stmt->fetch()) {
            $result = $row;
        }
        if ($result[0]) {
            print_r($result[0]); 
        } else {
            $errorArray = array("error" => "empty");
            $error = json_encode($errorArray);;
            print_r($error);
        }
    } catch (PDOException $e2) {
        echo 'Error: ' . $e2->getMessage();
    }

}