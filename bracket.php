<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="title" content="World of Brackets">
    <meta name="description" content="You can decide which are the best in brackets of all kinds.">
    <meta name="keywords"
        content="world, brackets, championship, bracket, choose, games, fps, brazil, brasil, play, twitch, chat, best brackets">
    <meta name="robots" content="index, follow">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="MrGuinas">

    <meta name="googlebot" content="index, follow" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>World of Brackets - <?= $idBracket ?> Bracket</title>
    <link rel="icon" type="image/png" href="../img/wobrackets.png" />
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="../node_modules/jquery-bracket/dist/jquery.bracket.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../node_modules/jquery-bracket/dist/jquery.bracket.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <script src="https://gitcdn.xyz/cdn/tmijs/cdn/e59ebeab0b5eb22e7bff9e053d8dbad58e7d030f/latest/1.x/tmi.min.js">
    </script>
    <script src="../js/twitch.js?v10"></script>
    <script src="../js/main.js?v10"></script>
    <link rel="stylesheet" type="text/css" href="../main.css" />
    <script type="text/javascript"
        src="https://platform-api.sharethis.com/js/sharethis.js#property=5ce82d51af7d000012cb0c65&product=inline-share-buttons">
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117547186-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-117547186-1');
    </script>

    <meta property="og:title" id="ogTitle" content="<?= $idBracket ?> Bracket">
    <meta property="og:description" id="ogDescription" content="You decide!">
    <meta property="og:image" id="ogImg" content="https://worldofbrackets.com//img/<?= $idBracket ?>/Cellbit.jpg">
    <meta property="og:url" id="ogUrl" content="https://worldofbrackets.com/<?= $idBracket ?>/">

</head>

<body>
    <br>
    <div class="container">

        <div class="row">
            <div class="col-12">
                <h3><a href="https://worldofbrackets.com"><img src="../img/wobrackets.png" height='80px'
                            alt="World of Brackets"></a> World of Brackets</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading" id="title"><i class="fas fa-spinner fa-spin"></i></h4>
                    <p id="subtitle"><i class="fas fa-spinner fa-spin"></i></p>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-lg-6"></div>
            <div class="col-lg-2"> <input type="checkbox" class="form-check-input" id="twitchCheck">
                <label class="form-check-label" for="twitchLabel" id="twitchCheckLabel"><i
                        class="fab fa-twitch"></i></label></div>
            <div class="col-lg-2 col-6 text-center">
                <button type="button" class="btn btn-outline-secondary btn-sm" id="btnShuffle"><i
                        class="fas fa-random"></i> Shuffle</button>
            </div>
            <div class="col-lg-2 col-6 text-center">
                <button type="button" class="btn btn-outline-danger btn-sm" id="btnReset"><i class="fas fa-undo"></i>
                    Reset</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <div id="bracket-div" class="text-center">
                    <i class="fas fa-spinner fa-3x fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="winnerModal" tabindex="-1" role="dialog" aria-labelledby="winnerModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="winnerModalTitle">Winner</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <img src="" alt="" id="imgWinner" height='300px'>
                    <br>
                    <h3 class="text-center" id="txtWinner"></h3>
                    <br><br>
                    <div class="sharethis-inline-share-buttons" id="shareThis"
                        data-url="https://worldofbrackets.com/<?= $idBracket ?>" data-title="Sharing is great!"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="chatPlayModal" tabindex="-1" role="dialog" aria-labelledby="chatPlayModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title inline" id="chatPlayModalTitle">Chat Play</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center" id="chatPlayModalBody">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 inline">
                                <div id="totalVotes">Votes: 0</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <b>Type: 1</b>
                            </div>
                            <div class="col-6">
                                <b>Type: 2</b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <img src="" id="chatPlayModalItem1" height='100' class='img-fluid'>
                            </div>
                            <div class="col-6">
                                <img src="" id="chatPlayModalItem2" height='100' class='img-fluid'>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6" id="name1">
                                -
                            </div>
                            <div class="col-6" id="name2">
                                -
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-6">
                                <h3 id="resultItem1">0%</h3>
                            </div>
                            <div class="col-6">
                                <h3 id="resultItem2">0%</h3>
                            </div>
                        </div>
                        <div class="row"><br></div>
                        <div class="row">
                            <div class="col-12">
                                <button type="button" class="btn btn-primary" id="btnChatPlayModalStart">Start
                                    Poll</button>
                                <button type="button" class="btn btn-success" id="btnChatPlayModalStop"
                                    style="display:none">End
                                    Poll</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="chatTwitchModal" tabindex="-1" role="dialog" aria-labelledby="chatTwitchModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="chatTwitchModalTitle"><i class="fab fa-twitch"></i> Twitch -
                        ChatPlay</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center" id="chatTwitchModalBody">
                    <div class="text-left">
                        In Chatplay your audience from Twitch can decide for you which option is better. Our site will
                        be
                        listening to your channel's chat, show which option has the majority of votes and select the
                        chosen
                        one.
                        <br>
                        <br>
                        Connect your twitch channel to play
                        <br>
                        <br>
                    </div>
                    <input type="text" class="form-control focusedInput" id="chatTwitchChannel"
                        aria-describedby="chatTwitchChannelHelp" placeholder="Enter twitch channel"><br>
                    <button type="button" class="btn btn-primary" id="btnChatTwitchChannel">Connect to channel</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>