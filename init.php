<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="title" content="World of Brackets">
    <meta name="description" content="You can decide which are the best in brackets of all kinds.">
    <meta name="keywords"
        content="world, brackets, championship, bracket, choose, games, fps, brazil, brasil, play, twitch, chat, best brackets">
    <meta name="robots" content="index, follow">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="MrGuinas">

    <meta name="googlebot" content="index, follow" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <title>World of Brackets - Bracket</title>
    <link rel="icon" type="image/png" href="../img/wobrackets.png" />
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="../node_modules/jquery-bracket/dist/jquery.bracket.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../node_modules/jquery-bracket/dist/jquery.bracket.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
    <script type="text/javascript"
        src="https://platform-api.sharethis.com/js/sharethis.js#property=5ce82d51af7d000012cb0c65&product=inline-share-buttons">
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117547186-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-117547186-1');
    </script>


    <meta property="og:title" id="ogTitle" content="<?= $idBracket ?> Bracket">
    <meta property="og:description" id="ogDescription" content="You decide!">
    <meta property="og:image" id="ogImg" content="https://worldofbrackets.com//img/wobrackets.png">
    <meta property="og:url" id="ogUrl" content="https://worldofbrackets.com/">



</head>

<body>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3><img src="../img/wobrackets.png" height='80px' alt="World of Brackets"> World of Brackets</h3>
            </div>
        </div>
        <?php
        $string = file_get_contents("brackets.json");
        $bracketsJson = json_decode($string);
        //echo "Hi, choose:<br><br>"; 
        $iCards = 0;
        // print '<div class="card-columns">';
        // print '<div class="card-deck">';
        print '<div class="row">';
        foreach ($bracketsJson->brackets as &$element) {
            if ($element->show) {
                ++$iCards;
                print '<div class="col-12 col-md-6 col-lg-3">';
                print "      <div class=\"card\">\n";
                print "        <img src=\"" . $element->og_img . "\" class=\"card-img-top\" alt=\"...\">\n";
                print "        <div class=\"card-body\">\n";
                print "          <h5 class=\"card-title\">" . $element->og_title . "</h5>\n";
                print "          <p class=\"card-text\">" . $element->og_description . "</p>\n";
                print "          </div>\n";
                print "          <div class=\"card-footer\"><a href=\"https://worldofbrackets.com/" . $element->idBracket . "/\" class=\"btn btn-primary\">Go " . $element->idBracket . "</a></div>\n";
                print "        </div>\n";
                print '</div>';
            }
        }
        //print "</div>";
        print "</div>";
        ?>
    </div>
</body>

</html>