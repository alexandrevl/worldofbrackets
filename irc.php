<?php

echo "Teste";
$server = 'ssl://example.com';
$port = 12345;
$pass = 'pass';
$nick = 'user';
$channel = '#channel';

function send( $conn, $msg )
{
	echo "--> {$msg}\n";
	return fwrite( $conn, $msg . "\n" );
}


$connection = fsockopen($server, $port, $errno, $errstr);

if ( ! $connection)
{
	die("$errstr ($errno)\n");
}

send( $connection, sprintf("PASS %s", $pass) );
send( $connection, 'USER username "1" "1" :username_again');
send( $connection, sprintf("NICK %s", $nick) );
//send( $connection, sprintf("JOIN %s", $channel) ); // Can't join yet...

$joined = false;

	while ( $input = trim( fgets( $connection ) ) )
	{
		stream_set_timeout( $connection, 3600 );

		print $input . "\n";

		// Join after all is well
		if(strpos($input, "266") AND ! $joined)
		{
			send( $connection, sprintf("JOIN %s", $channel));
			$joined = TRUE;
			continue;
		}

		// Keep alive
		if(preg_match("|^PING :(.*)$|i", $input, $matches))
		{
			echo "[ SERVER PING {$matches[1]} ]\n";
			send( $connection, "PONG :{$matches[1]}" );
			continue;
		}

		$regex = '(:[^ ]+) ([A-Z]*) #(.+?) :([^\n]+)';
		if(preg_match("~$regex~", $input, $match))
		{
			print "Message to ". $match[3] . ': '. $match[4]. "\n";
		}

		/*
		switch( true )
		{
			//422 is the message number of the MOTD for the server (The last thing displayed after a successful connection)
			case(strpos($input, "266")):
				send( $connection, sprintf("JOIN %s", $channel));
			break;
			//keep alive
			case( preg_match("|^PING :(.*)$|i", $input, $matches ) ):
				echo "[ SERVER PING {$matches[1]} ]\n";
				send( $connection, "PONG :{$matches[1]}" );
			break;
			//messages with recipients
			case( preg_match("|^:(?P<from>.+?)!.* (?P<cmd>[A-Z]*) (?P<to>.+?) :(?P<msg>.*)$|i", $input, $matches ) ):
				printf( "%-12s%s -> %s: %s\n", $matches["cmd"], $matches["from"], $matches["to"], $matches["msg"] );
			break;
			//messages without recipients
			case( preg_match("|^:(?P<from>.+?)!.* (?P<cmd>[A-Z]*) :(?P<msg>.*)$|i", $input, $matches ) ):
				printf( "%-12s%s <- %s\n", $matches["cmd"], $matches["msg"], $matches["from"] );
			break;
			//kick everything else out to our shell
			default:
				echo $input, "\n";
			break;
		}
		*/

	}