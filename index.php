<html>

<head>
    <title>
        World of Brackets
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Cache-control" content="public">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name=apple-mobile-web-app-capable content=yes>
    <meta name=apple-mobile-web-app-status-bar-style content=white>
    <meta name=description
        content="The best place to see if you and your friends have (or not) the same ideas! Try NOW!">
</head>
<?php
require 'AltoRouter.php';
$router = new AltoRouter();
$idBracket = null;  
$data = null;

$string = file_get_contents("brackets.json");
$bracketsJson = json_decode($string);
$index = 0;
foreach ($bracketsJson->brackets as &$element) {
    //var_dump($element->idBracket); 
    // echo "-";
    $router->map( 'GET', '/'. $element->idBracket . '/', function() {
        require __DIR__ . '/bracket.php';
    });
}

$router->map( 'GET', '/', function() {
    require __DIR__ . '/init.php';
    //require __DIR__ . '/bot/twitch/index.php'; 
}); 

$router->map( 'GET', '/dicoi', function() { 
    require __DIR__ . '/dicoi.html';
    //require __DIR__ . '/bot/twitch/index.php'; 
}); 

// $router->map( 'GET', '/api/setBracket/'. $element->idBracket . '/', function() {
//     require __DIR__ . '/setBracket.php';
// });

$match = $router->match();


// call closure or throw 404 status
if( is_array($match) && is_callable( $match['target'] ) ) {
	call_user_func_array( $match['target'], $match['params'] ); 
} else {
	// no route was matched
    header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found'); 

}

?>

</html>