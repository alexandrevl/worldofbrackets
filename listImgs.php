<?php
$idBracket = $_GET["idBracket"];
$path = 'img/' . $idBracket;
if ($handle = opendir($path)) {
    $array = array();
    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {
        if (strlen($entry) > 2) {
            array_push($array, $idBracket . "/" . $entry);
        }
    }
    closedir($handle);
    echo json_encode($array);

}
?>